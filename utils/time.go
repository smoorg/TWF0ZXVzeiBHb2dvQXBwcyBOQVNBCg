package utils

import (
	"fmt"
	"time"
)

func StringToDate(date string) (time.Time, error) {
	t, err := time.Parse("2006-01-02", date)
	if err != nil {
		return time.Now(), err
	}

	return t, nil
}


// adds zero to date parts in case of month/day is less than 10
func AddZero(v int) string {
	if v < 10 {
		return fmt.Sprintf("0%v", v)
	}
	return fmt.Sprintf("%v", v)
}
