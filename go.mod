module github.com/smoorg/gogo-space

go 1.19

require (
	github.com/go-chi/chi/v5 v5.0.7
	github.com/unrolled/render v1.5.0
)

require (
	github.com/ajg/form v1.5.1 // indirect
	github.com/gammazero/deque v0.2.0 // indirect
)

require (
	github.com/fsnotify/fsnotify v1.5.4 // indirect
	github.com/gammazero/workerpool v1.1.3
	github.com/go-chi/render v1.0.2
	github.com/gorilla/mux v1.8.0
	github.com/pkg/errors v0.9.1
	golang.org/x/sys v0.0.0-20220615213510-4f61da869c0c // indirect
)
