package main

import (
	"fmt"
	"net/http"

	"github.com/smoorg/gogo-space/env"
	"github.com/smoorg/gogo-space/api"
)

func main() {
    e := env.GetEnv()

    fmt.Println("App starts...")
    fmt.Println("Environment:")
    fmt.Printf("\tPORT=%v\n",e.Port)
    fmt.Printf("\tCONCURRENCY_REQUESTS=%v\n",e.ConcurentRequests)
    fmt.Printf("\tAPI_KEY=%v\n",e.ApiKey)

    r := api.ApiRouter(e)

    url := fmt.Sprintf(":%v", e.Port)
	http.ListenAndServe(url, r)
}
