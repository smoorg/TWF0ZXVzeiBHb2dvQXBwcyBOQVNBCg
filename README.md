# TWF0ZXVzeiBHb2dvQXBwcyBOQVNBCg

# Run

```sh
$ go build .
$ ./gogo-space
```

And in separated window you can test it by prepared `run.sh` script which just call API with prepared query params.
```sh
$ ./run.sh
```

If that won't work, add executable flag if necessary.
```sh
$ chmod +x ./run.sh
```

# Test

The only tests for now are in api/pictures but `go test` has troubles with recognize that.

```sh
$ go test ./api/pictures
```
