package api

import (
	"net/http"

	chi "github.com/go-chi/chi/v5"
	"github.com/smoorg/gogo-space/env"
	"github.com/smoorg/gogo-space/api/pictures"
)

func ApiRouter(e *env.Environment) http.Handler {
	r := chi.NewRouter()

	r.Route("/pictures", func(r chi.Router) { 
        pictures.PicturesRouter(r, e) 
    })

	return r
}
