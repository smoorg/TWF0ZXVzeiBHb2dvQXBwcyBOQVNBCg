package pictures

import (
	"net/http"

	chi "github.com/go-chi/chi/v5"
	env "github.com/smoorg/gogo-space/env"
)

func PicturesRouter(r chi.Router, e *env.Environment) {
	r.Get("/", func(w http.ResponseWriter, req *http.Request) {
		getPictures(w, req, e)
	})
}
