package pictures

import (
	"net/http"
	"testing"

	env "github.com/smoorg/gogo-space/env"
)

func TestGetParams(t *testing.T) {
    r,_ := http.NewRequest("GET", "/test?from=2020-01-01&to=2020-01-01", nil)
    from, to, err := getPicturesParams(r)
    if err != nil {
        t.Error(err)
    }

    if from.Year() != 2020 || int(from.Month()) != 01 || from.Day() != 1 {
        t.Error("Should parse 'from' param correctly")
    }

    if to.Year() != 2020 || int(to.Month()) != 01 || to.Day() != 1 {
        t.Error("Should parse 'to' param correctly")
    }
}

func TestGetSingleNasaPicture(t *testing.T) {
    r,_ := http.NewRequest("GET", "/pictures?from=2020-01-01&to=2020-01-01", nil)
    from, to, err := getPicturesParams(r)
    if err != nil {
        t.Error(err)
    }

    e := &env.Environment{
        Key: "DEMO_KEY",
        ConcurentRequests: 5,
        Port: 3333,
    }

    result, err := getNasaPictures(e, from, to)
    if err != nil {
        t.Error(err)
    }

    if  len(result) != 1 {
        t.Errorf("Should call api exactly once, called it %v times.\n", len(result))
    }
}


func TestGetTwoNasaPictures(t *testing.T) {
    r,_ := http.NewRequest("GET", "/pictures?from=2020-01-01&to=2020-01-02", nil)
    from, to, err := getPicturesParams(r)
    if err != nil {
        t.Error(err)
    }

    e := &env.Environment{
        Key: "DEMO_KEY",
        ConcurentRequests: 5,
        Port: 3333,
    }

    result, err := getNasaPictures(e, from, to)
    if err != nil {
        t.Error(err)
    }

    if  len(result) != 2 {
        t.Errorf("Should call api exactly twice, called it %v times.\n", len(result))
    }
}
