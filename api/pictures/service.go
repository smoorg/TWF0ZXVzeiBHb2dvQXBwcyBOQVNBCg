package pictures

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/gammazero/workerpool"
	"github.com/go-chi/render"
	env "github.com/smoorg/gogo-space/env"
	timeUtils "github.com/smoorg/gogo-space/utils"
)

// GET /pictures?from=2020-01-04&to=2020-02-05
func getPictures(w http.ResponseWriter, r *http.Request, e *env.Environment) {
	type response struct {
		Urls  []string `json:"urls,omitempty"`
		Error string   `json:"error,omitempty"`
	}

	from, to, err := getPicturesParams(r)
	if err != nil {
		render.JSON(w, r, fmt.Sprintf("{error: %s}", err.Error()))
	}

	res := response{}

	if pictures, err := getNasaPictures(e, from, to); err != nil {
		res.Error = err.Error()
    	render.Status(r, 400)
	} else {
		res.Urls = pictures
	}

	render.JSON(w, r, res)
}

func getPicturesParams(r *http.Request) (time.Time, time.Time, error) {
	qfrom := r.URL.Query().Get("from")
	qto := r.URL.Query().Get("to")

	from, err := timeUtils.StringToDate(qfrom)
	if err != nil {
		return time.Now(), time.Now(), err
	}

	to, err := timeUtils.StringToDate(qto)
	if err != nil {
		return from, to, err
	}

	if from.Unix() > to.Unix() {
		return from, to, errors.New("Invalid timespan. 'to' date should be after 'from'")
	}

	return from, to, nil
}


// Gets list of images or error if any request failed
func getNasaPictures(e *env.Environment, from time.Time, to time.Time) ([]string, error) {
	cur := from
	pictures := make([]string, 0)

	wp := workerpool.New(e.ConcurentRequests)
	mu := &sync.Mutex{}
	errMsg := ""

	dur := to.Sub(from)
	durDays := int(dur.Abs().Hours() / 24)

	for i := 0; i <= durDays; i++ {
		cur = cur.AddDate(0, 0, 1)
		wp.Submit(func() {
			client := new(http.Client)
			resp, respErr := getSingleNasaPicture(client, e.Key, cur)

			mu.Lock()
			if respErr != nil {
				errMsg = respErr.Error()
			} else {
				pictures = append(pictures, resp)
			}

			mu.Unlock()
		})
	}

	wp.StopWait()

	if errMsg != "" {
		return []string{}, errors.New(errMsg)
	}

	return pictures, nil
}

// Returns single image or error if something went wrong
func getSingleNasaPicture(client *http.Client, key string, date time.Time) (string, error) {
	year, month, day := date.Date()

	u := fmt.Sprintf(
		"https://api.nasa.gov/planetary/apod?api_key=%s&date=%v-%v-%v",
		key,
		year,
		timeUtils.AddZero(int(month)),
		timeUtils.AddZero(day),
	)

	resp, respErr := client.Get(u)
	if respErr != nil {
		return "", respErr
	}

	switch resp.StatusCode {
	case 429:
		return "", errors.New("Too many requests")
	case 200:
		fallthrough
	default:
	}

	if resp.Body != nil {
		defer resp.Body.Close()
	}

	url, readErr := getUrl(resp)
	if readErr != nil {
		return "", readErr
	}

	return url, nil

}

// Parses response from NASA API to get string URL from JSON body
func getUrl(resp *http.Response) (string, error) {
	type jsonResp struct {
		Url string `json:"url"`
	}
	jsonBody := jsonResp{}

	readErr := json.NewDecoder(resp.Body).Decode(&jsonBody)
	if readErr != nil {
		return "", readErr
	}

	return jsonBody.Url, nil
}
