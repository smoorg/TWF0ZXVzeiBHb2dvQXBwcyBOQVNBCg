package env

import (
	"os"
	"strconv"
)

type Environment struct {
	Key               string
	ConcurentRequests int
	Port              int
}

func GetEnv() *Environment {
	return &Environment{
		Key:               getKey(),
		Port:              getPort(),
		ConcurentRequests: getRequestLimit(),
	}
}

func getPort() int {
	portEnv, hasValue := os.LookupEnv("PORT")
	if hasValue != false {
		return 3333
	}

	v, err := strconv.Atoi(portEnv)
	if err != nil {
		return 3333
	}

	return v
}
func getKey() string {
	apiKey := "DEMO_KEY"
	if apiKeyEnv, hasValue := os.LookupEnv("API_KEY"); hasValue != false {
		apiKey = apiKeyEnv
	}

	return apiKey
}

func getRequestLimit() int {
	concurrent_requests := 5
	crEnv, hasValue := os.LookupEnv("CONCURRENT_REQUESTS")
	if hasValue != false {
		if v, err := strconv.Atoi(crEnv); err == nil {
			concurrent_requests = v
		}
	}

	return concurrent_requests
}
